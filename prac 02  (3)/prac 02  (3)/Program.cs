﻿using System;

namespace prac_02___3_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<<<<<<<<Bienvenidos a nuestro programa>>>>>>>>>>>>>>>");
            Console.WriteLine();
            Console.WriteLine("Crear un programa que dé al usuario tres oportunidades para adivinar un número del 1 al 23.");
            Console.WriteLine();

            int number;
            int number1 = 23;
            int contador = 0;

            Console.WriteLine("Adivina un numero del 1 al 23");
            number = Convert.ToInt32(Console.ReadLine());

            if (number == number1)
                Console.WriteLine("haz acertado el numero");
            else
                while ((number != number1) && (contador <= 1))
                {
                    Console.WriteLine("Numero es incorrecto");

                    Console.WriteLine("Adivinar un numero del 1 al 23");
                    number = Convert.ToInt32(Console.ReadLine());

                    if (number == number1)
                        Console.WriteLine("Numero correcto");

                    contador = contador + 1;
        }
            Console.WriteLine();
            Console.WriteLine("Gracias por su tiempo");
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
