﻿using System;

namespace prac_02__4_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<<<<<<<<Bienvenidos a nuestro programa>>>>>>>>>>>>>>>");
            Console.WriteLine();
            Console.WriteLine("Crea un programa que escriba en pantalla los números del 1 al 10, usando dowhile");
            Console.WriteLine();

            int number = 0;
            
            Console.WriteLine("iniciar el conteo");
            
            do
            {
                Console.WriteLine(number);
                number++;
            } while (number <= 10);
            Console.WriteLine();
            Console.WriteLine("Gracias por su tiempo");
            Console.ReadKey();
    }   }

}
