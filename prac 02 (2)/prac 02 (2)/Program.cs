﻿using System;

namespace prac_02__2_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<<<<<<<<Bienvenidos a nuestro programa>>>>>>>>>>>>>>>");
            Console.WriteLine();
            Console.WriteLine("Crear un programa calcule cuantas cifras tiene un número entero positivo (pista: se puede hacer dividiendo varias veces entre 10).");
            Console.WriteLine();

            int numero;
            int cifras = 0;
            

            Console.Write("Introduce un número entero positivo: ");
            numero = Convert.ToInt32(Console.ReadLine());

            while (numero >= 1)
            {
                Console.WriteLine(numero);

                numero =numero / 10;
                cifras++;
            }
            Console.WriteLine("las cifras son" + cifras);
            Console.WriteLine();
            Console.WriteLine("Gracias por su tiempo");
            Console.ReadKey();
            
        }
    }
}