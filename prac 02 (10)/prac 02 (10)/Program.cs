﻿using System;

namespace prac_02__10_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<<<Bienbenidos a nuestro programa>>>>>>>>>>>> \n");
            Console.WriteLine("Crear un programa que pregunte al usuario su edad y su año de nacimiento \n");

            try
            {
                int edad;
                Console.WriteLine(" Que edad tienes? ");
                edad = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine(" Hubo un error de formato");
            }

            int año;
            Console.WriteLine("Introduce tu fecha de nacimiento");
            año = int.Parse(Console.ReadLine());

            Console.ReadKey();
        }
    }
}
