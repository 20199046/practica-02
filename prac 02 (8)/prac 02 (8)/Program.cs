﻿using System;

namespace prac_02__8_
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("<<<<<<<<<<Bienbenidos a nuestro programa>>>>>>>>>>>> \n");
            Console.WriteLine("Crear un programa que muestre las letras de la Z (mayúscula) a la A (mayúscula, descendiendo) \n");

            char letra;

            for (letra = 'Z'; letra >= 'A'; letra--)
                Console.Write("{0} ", letra);

            Console.ReadKey();
        }
        
    }
}
    

