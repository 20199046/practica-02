﻿using System;
using System.Reflection;

namespace prac_02__5_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<<<<<<<<Bienvenidos a nuestro programa>>>>>>>>>>>>>>>");
            Console.WriteLine();
            Console.WriteLine("Crear un programa que pida números positivos al usuario, y vaya calculando la suma de todos ellos");
            Console.WriteLine();

            int number = 0, contador = 0;

            do
            {

                contador = int.Parse(Console.ReadLine());
                number = number + contador;

            }
            while (contador >= 1);
            Console.WriteLine(number);

            Console.ReadKey();
        }
    }
}
